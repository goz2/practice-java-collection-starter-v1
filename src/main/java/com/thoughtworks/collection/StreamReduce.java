package com.thoughtworks.collection;

import java.util.List;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {
        return numbers.stream().filter(number -> (number & 1) == 1).reduce((a, b) -> b).orElse(0);
    }

    public String getLongest(List<String> words) {
        return words.stream().reduce((a, b) -> a.length() > b.length() ? a : b).orElse("");
    }

    public int getTotalLength(List<String> words) {
        return words.stream().map(String::length).reduce(Integer::sum).orElse(0);
    }
}
